{-# LANGUAGE OverloadedStrings #-}

module Media.Subtitles.SRT.Formatting
  ( linef,
    srtf,
  )
where

import Formatting
import Media.Subtitles.SRT
import Media.Timestamp.Formatting

-- | Formats a `Line` to SRT format.
--
-- @since 0.1.0.0
linef :: Format r (Line -> r)
linef = accessed index int % "\n" <> accessed range (rangeaf timecf) % "\n" <> accessed dialog stext % "\n"

-- | Formats an entire collection of lines to SRT format.
--
-- @since 0.1.0.0
srtf :: Format r (SRT -> r)
srtf = accessed unSRT (intercalated "\n" linef)
