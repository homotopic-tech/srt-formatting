# srt-formatting

Formatting for [srt](https://hackage.haskell.org/package/srt)
using the [formatting](https://hackage.haskell.org/package/formatting) library.
